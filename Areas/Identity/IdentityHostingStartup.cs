﻿using System;
using BestGameWebSite.Areas.Identity.Data;
using BestGameWebSite.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(BestGameWebSite.Areas.Identity.IdentityHostingStartup))]
namespace BestGameWebSite.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<BestGameWebSiteContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("BestGameWebSiteContextConnection")));

                services.AddDefaultIdentity<BestGameWebSiteUser>(options => options.SignIn.RequireConfirmedAccount = true)
                    .AddEntityFrameworkStores<BestGameWebSiteContext>();
            });
        }
    }
}